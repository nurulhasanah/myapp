import * as React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

export default class Login extends React.Component{
    render(){
        return(
            <View style={styles.container}>
                <Text style={styles.text}>Welcome Back!</Text>
                <Text style={styles.text1}>Sign in to continue</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Username"
                />

                <TextInput
                    style={styles.input}
                    placeholder="Password"
                />

                <View style={styles.form}>
                    <TouchableOpacity style={styles.tombol}>
                        <Text style={styles.text2}>Login Now</Text>
                    </TouchableOpacity>

                    <Text style={{ marginTop: 20 }}>Forgot Password?</Text>

                    <View style={styles.akun}>
                        <View style={styles.fb}>
                            <Text style={styles.fbtext}>f</Text>
                        </View>
                        <View style={styles.g}>
                            <Text style={styles.gtext}>G</Text>
                        </View>
                        <View style={styles.tw}>
                            <Text style={styles.twtext}>t</Text>
                        </View>
                    </View>

                    <View style={styles.akun1}>
                    <Text style={{ color: 'white' }}>Don't have an account?</Text>
                    <Text style={{ fontWeight: 'bold' }}>Sign Up</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ad1457',
        padding: 20,
    },

    text: {
        fontSize: 25,
        marginTop: 20
    },

    text1: {
        fontSize: 16,
        color: 'white',
        marginTop: 20
    },

    input: {
        marginTop: 40,
        borderBottomColor:'#ddd',
        borderBottomWidth: 1,
        paddingBottom: 20
    },

    form: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40
    },

    tombol: {
        width: 200,
        backgroundColor: '#ba68c8',
        padding: 10,
        alignItems: 'center',
        borderRadius: 40, marginTop: 30
    },

    text2: {
        textAlign: 'center',
        color : 'white',
        fontSize: 16,
    },

    akun: {
        flexDirection: 'row',
        marginTop: 25
    },

    fb: {
        height: 40,
        width: 40,
        borderRadius: 40/2,
        backgroundColor: '#3f51b5',
        alignItems: 'center',
        justifyContent: 'center',
    },

    g: {
        height: 40,
        width: 40,
        borderRadius: 40/2,
        backgroundColor: '#f44336',
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
    },

    tw: {
        height: 40,
        width: 40,
        borderRadius: 40/2,
        backgroundColor: '#1565c0',
        alignItems: 'center',
        justifyContent: 'center',
    },

    fbtext: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
    },

    gtext: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
    },

    twtext: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'white',
    },

    akun1: {
        flexDirection: 'row',
        marginTop: 20
    }
})