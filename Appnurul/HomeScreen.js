import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import logo from './assets/favicon.png'

export default class HomeScreen extends React.Component {
  render(){

    this.props.navigation.setOptions({
        headerBackTitle: '',
        headerShown: false,
    })



  return (
    <View style={styles.container}>
      <Image
        source={logo}
        style={styles.logo}
      />

      <Text style={styles.text}>Hello!</Text>
      <Text style={styles.text1}>Selamat Datang di Appnurul</Text>

      <View style={styles.form}>
        <TouchableOpacity 
          onPress={ () => this.props.navigation.navigate('Login') }
          style={styles.input}>
            
          <Text style={styles.text2}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.input}>
          <Text style={styles.text2}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ad1457',
    alignItems: 'center',
    justifyContent: 'center',
  },

  logo: {
    width: 120,
    height: 120,
    alignItems: 'center',
    marginBottom: 25,
  },

  text: {
    fontSize: 40,
    fontWeight: 'bold'
  },

  text1: {
    fontSize: 16,
    color: 'white',
    textAlign: 'center',
    marginHorizontal: 20
  },

  form:{
    flexDirection: 'row',
    margin: 20,
    paddingVertical: 20
  },

  input: {
    backgroundColor:'#ba68c8',
    padding: 10,
    width: 150,
    borderRadius: 30,
    marginHorizontal: 2,
    borderColor: '#000000',
    borderWidth: 1
  },

  text2: {
    textAlign: 'center',
    color: 'black',
    fontSize: 18
  }
});
